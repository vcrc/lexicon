from Fetcher import Fetcher

pagesToFetch = []
pagesToFetch.append("https://www.bbc.com/mundo/internacional")
pagesToFetch.append("https://actualidad.rt.com/todas_las_noticias")
pagesToFetch.append("https://tec.mx/es/noticias/nacional")
pagesToFetch.append("https://www.elimparcial.com/tijuana/")
pagesToFetch.append("http://www.cetys.mx/noticias/")

fetch = Fetcher(pagesToFetch)

fetch.getLinks()
fetch.getText()
fetch.writeDictionary() # writes words list to .txt file

from stemmer import Stemmer

s = Stemmer('spanish') # define stemmer and dictionary's language
s.execute() # find and order stems into dictionary

s.readStems() # loads dictionary from .txt file for further usage
