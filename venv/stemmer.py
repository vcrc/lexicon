import nltk

class Stemmer:

    # variable definitions within class
    def __init__(self, language):
        self.language = language
        self.stemmer = nltk.SnowballStemmer(language)
        self.stems = dict()

    # gets stem of each word and stores it in dictionary
    # creates dictionary | stem: words |
    def makeStems(self, words):
        stems = self.stems
        for word in words:
            if self.stemmer.stem(word) in stems:
                stems[self.stemmer.stem(word)] = stems[self.stemmer.stem(word)] + ',' + word
            else:
                stems[self.stemmer.stem(word)] = word
        self.setStems(stems)

    # saves dictionary in a file
    def writeStems(self, stems):
        with open("stems.txt", 'w') as f:
            for key, value in stems.items():
                f.write('%s:%s\n' % (key, value))

    # reads dictionary file and loads into function
    def readStems(self):
        stems = dict()
        with open("stems.txt", 'r') as f:
            lines = f.readlines()
            for line in lines:
                key, values = line.split(':')
                words = values.strip('\n').split(',')
                stems[key] = words
        return stems

    # excecution of previous functions for dictionary initialization
    def execute(self):
        with open('dict.txt', 'r') as f:
            words = [line.strip() for line in f]
        self.makeStems(words)
        self.writeStems(self.stems)
        self.stems = self.readStems()

        return
    
    # dictionary setter
    def setStems(self, stems):
        self.stems = stems
    
    # dictionary getter
    def getStems(self):
        return self.stems
