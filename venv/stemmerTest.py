from stemmer import Stemmer

s = Stemmer('spanish')
s.execute() # Stemmer initialization

# stems = s.readStems() # Reads stems file
stems = s.getStems() # Loads stem dictionary
print(len(stems)) # Prints the lenght of the dictionary