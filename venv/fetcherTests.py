from Fetcher import Fetcher

paginas = []    #Creamos lista de paginas a utilizar
paginas.append("https://www.bbc.com/mundo/internacional") #Agregamos paginas a la lista
fetcher = Fetcher(paginas) #Creamos una clase fetcher con la lista de paginas 
fetcher.getLinks()          #Obtenemos los links que se encuentran dentro de la pagina
fetcher.getText()           #Obtenemos las palabras que estan en la pagina de todos los links obtenidos
fetcher.writetoDict()       #Agregamos el diccionario nuevo al archivo txt
print(len(fetcher.dictionary))   #Imprimimos el tamaño de la lista de palabras encontradas