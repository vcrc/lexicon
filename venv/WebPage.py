import re
import urllib.request
from bs4 import BeautifulSoup

#Tag Regex
TAG_RE = re.compile(r'<[^>]+>')
#Not Letters Regex
NOT_LETTERS = re.compile(r'[^a-zA-ZÀ-ú]+')
#Acronyms Regez
ACRONYMS = re.compile(r'\b(?:[A-Z][a-z]*){2,}')

class WebPage:
    def __init__(self, url, level=0, text=None, anchorTags=None, html=None, httperror=None):
        self.url = url
        self.level = level
        self.text = ""
        try:
            self.html = urllib.request.urlopen(self.url).read()
            self.httperror =  0
        except urllib.error.HTTPError as e:
            self.httperror =  e.code
        self.anchorTags = []

    #get Html from Given WebPage
    def getHTML(self):
        soup = BeautifulSoup(self.html, 'html.parser')
        [s.extract() for s in soup(['style', 'script', '[document]', 'head', 'title'])]
        self.text = soup.getText()
     
    # Returns all anchor tags
    def getAnchorTags(self):
        soup = BeautifulSoup(self.html, 'html.parser')

        aTags = [a['href'] for a in soup.select('a[href]') if len(a['href']) > 1 and (re.findall('^http', a['href']) or re.findall('^/', a['href']))]

        self.anchorTags = aTags


    """ Functions to apply Regex """

    def removeTags(self, text):
        return TAG_RE.sub('', text)

    def removeNotCharacters(self, text):
        return NOT_LETTERS.sub(' ', text)

    def removeAcronyms(self, text):
        return ACRONYMS.sub(' ', text)


    # Return list of clean text
    def getList(self, text):
        result = text.split()
        resulstClean = list(filter(lambda x: len(x) > 1, result))
        for item in resulstClean:
         if(len(item) > 20):
             resulstClean.remove(item)
        return resulstClean
"