import urllib
import re
import urllib.request
from bs4 import BeautifulSoup
from WebPage import WebPage

class Fetcher:
    def __init__(self, pages=None, dictionary=None, pageList=None):
        self.pages = pages
        self.dictionary = dictionary
        self.pageList = pageList

    # Function to take Text From HTML
    def getText(self):
        dictionary = self.dictionary
        for page in self.pageList:
            webPage = WebPage(page)
            if webPage.httperror == 0:
                webPage.getHTML()
                webPage.getAnchorTags()
                htmlText = webPage.removeTags(webPage.text)
                htmlText = webPage.removeNotCharacters(htmlText)
                htmlText = webPage.removeAcronyms(htmlText)
                newList = webPage.getList(htmlText)
                newList = list(dict.fromkeys(newList))
                newList = sorted(newList)
                if (dictionary):
                    dictionary.extend(newList)
        dictionary = list(dict.fromkeys(dictionary))
        dictionary = sorted(dictionary)
        self.dictionary = dictionary

    # Parser for anchor Tags in HTML
    def getLinks(self):
        pageList = []
        for pg in self.pages:
            web = urllib.request.urlopen(pg)
            soup = BeautifulSoup(web, 'html.parser')
            soup.prettify()
            for anchor in soup.findAll('a', href=True):
                if anchor['href'] != "#" and "https":
                    if re.findall("^/.*", anchor['href']):
                        pagina = re.sub('/((?!\.).)*$', '', pg)
                        url = pagina + anchor['href']
                        pageList.append(url)
                    elif re.findall("^http.*", anchor['href']):
                        url = anchor['href']
                        pageList.append(url)
                        
        pageList = list(dict.fromkeys(pageList))
        self.pageList = pageList

    #Write to file
    def writeDictionary(self):
        with open("dict.txt", 'w') as f:
            for item in self.dictionary:
                f.write('%s\n' % (item))

    #Returns Class dictionary
    def getDictionary(self):
        return self.dictionary

    # Get pages
    def getpageList(self):
        return self.pageList

