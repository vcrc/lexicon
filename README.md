# Lexicon
The Lexicon will work as a starting point for word searching serving a bank of dynamic words, this words will come from specific URLS where words may vary from time to time, after getting a bulk of words this will be ordered in a way where we can optimize word searching by getting the stem of the word and its respective suffix.

## Table of Contents
* [Project Statement](#project-statement)
* [Getting Started](#getting-started)
    * [Prerequisites](#prerequisites)
    * [Installing](#installing)
* [Usage](#usage)
* [Running the Tests](#running-the-tests)
    * [Tests](#tests) 
* [Built With](#built-with)
* [Authors](#authors)
* [License](#license)
* [Acknowledgment](#acknowledgements)

## Project Statement
A lexicon can be known as a branch or software that stores a bulk of words of a particular subject, this project will take that bulk of words and classify the given words into a structure which will make searching for words easier in a way where the particular root of a group of words will be stored with a list of the possible suffixes of that stem.

## Getting Started
to start using the project it can be download from the repo using:

```bash
git clone https://gitlab.com/vcrc/lexicon.git
```

there will be 2 main scripts that will be used:
 fetcher.py - does the webscraping
 (To be named) - Execute stemming and creates dictionary

### Prerequisites
To execute the given codes, you will first need:
* Python 3.4 or higher
* Access to Internet for Web Scraping
* Permissions to read, write and execute on given files

### Installing

To install any dependency for the environment  you wil just need to run the command: 
```bash
pip install -r requirements.txt --user
```

inside "venv", folder

## Usage

The project is made up of 2 important files. Like mentioned ,"fetcher.py" will be doing the webscraping, and stemmer.py will do the dictionary and stemming structure. If using command line just by typing in the terminal, python3 runner.py should
 start doing the web scraping , cleaning and start buidling the stems for the words collected from the web pages, if wanted to be done individually 
 a class structure was followed to just start using the methods from the classes.

![alt text](./code_snippets/fetchetSnippet.png)

![alt text](./code_snippets/stemmerSnippet.png)



```bash
python3 runner.py
```
## Tests

* Fetcher (fetcherTests.py)
    * Page Link Fetcher
    * Word Text Fetcher

* Stemmer (stemmerTest.py)
    * Stem recognition
    * Saving stem dictionary to text file
    * Reading stems dictionary from text file
    * Get stems



## Built With
* Python 3.4+
* numpy version 1.16.3
* nltk version 3.4.1
* bs4 (BeautifulSoup) version 0.0.1 


## Authors
* Diego Mendoza Coronado
* Felipe Guzman Preciado
* Jorge Luis Aragon Martinez
* Brian Felix de la Cerda

# Licencia
MIT License

Copyright (c) 2019 VCRC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


# Acknowledgements

* Willett, P, “The Porter stemming algorithm: then and now”. Program: electronic library and information systems, 40 (3). pp. 219-223. [Online]. Available: http://eprints.whiterose.ac.uk/1434/1/willettp9_PorterStemmingReview.pdf/. [Accessed May. 13, 2019].
* Bird, S., Klein, W., & Loper, E., Natural Language Processing with Python. CA: O’Reilly, 2009.
* Jabeen, A, “Stemming and Lemmatization in Python”. 2018 [Online]. Available: https://www.datacamp.com/community/tutorials/stemming-lemmatization-python/. [Accessed May. 6, 2019].
* Porter, M. “Spanish stemming algorithm”. 2018 [Online]. Available: https://snowballstem.org/algorithms/spanish/stemmer.html/. [Accessed May. 6, 2019].
